import requests
import json
import os.path
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA
import praw
from IPython import display
import re as regex
from datetime import datetime
import time

sia = None
cards_master = {}
EMPTY_CARD_MASTER_ITEM = {"mentions": 0,
                          "replies": 0,
                          "upvotes": 0,
                          "positive_reply_score": 0,
                          "neutral_reply_score": 0,
                          "negative_reply_score": 0,
                          "compound_score": 0}


def get_card_master_item(card):
    if card not in cards_master:
        item = {card: EMPTY_CARD_MASTER_ITEM.copy()}
        cards_master.update(item)

    item = cards_master[card]

    return item


def update_card_master_item(item, score, post_or_comment, update_mentions: False, update_replies: False):
    item["positive_reply_score"] += score["pos"]
    item["negative_reply_score"] += score["neg"]
    item["neutral_reply_score"] += score["neu"]
    item["compound_score"] += score["compound"]
    if update_mentions:
        item["mentions"] += 1
    if update_replies:
        item["replies"] += 1
    item["upvotes"] += post_or_comment.score


def find_cards_in_text(text):
    return regex.findall('(?<=\[\[).+?(?=\]\])', text)


def evaluate_replies(replies, found_cards_parent):
    replies.replace_more(limit=None)
    for comment in replies:
        sentiment_score = sia.polarity_scores(comment.body)
        for found_card in found_cards_parent:
            cards_master_item = get_card_master_item(found_card)
            update_card_master_item(cards_master_item, sentiment_score, comment, False, True)

        found_cards_comment = find_cards_in_text(comment.body)
        for found_card in found_cards_comment:
            cards_master_item = get_card_master_item(found_card)
            update_card_master_item(cards_master_item, sentiment_score, comment, True, False)
        evaluate_replies(comment.replies, found_cards_comment)


# todo: andere Subreddits als DB Column

def main():
    start_time = time.time()

    f = open(os.path.join(os.path.dirname(__file__), os.path.pardir, os.path.pardir, 'RedditScraper.json'))
    data = json.load(f)
    f.close()

    app_id = data['appId']
    secret = data['secret']
    user = data['user']
    pw = data['pw']
    agent = data['userAgent']

    subreddits = data['subreddits']

    reddit = praw.Reddit(
        client_id=app_id,
        client_secret=secret,
        user_agent=agent,
        password=pw,
        username=user
    )
    global sia
    sia = SIA()

    for subreddit in subreddits:
        subreddit_name = subreddit['name']
        print('starting subreddit', subreddit_name)
        # Warning: API limit of 1000 Posts :(
        # Hopefully, the interesting cards will be discussed in the top posts, so will still always be fetched
        for submission in reddit.subreddit(subreddit_name).top(time_filter="week", limit=None):

            post_title_body = submission.title + '.' + submission.selftext
            found_cards_post = find_cards_in_text(post_title_body)
            sentiment_score = sia.polarity_scores(post_title_body)
            for found_card in found_cards_post:
                cards_master_item = get_card_master_item(found_card)
                update_card_master_item(cards_master_item, sentiment_score, submission, True, False)

            evaluate_replies(submission.comments, found_cards_post)

            print('Created: ', datetime.fromtimestamp(submission.created_utc))
            print('Title:', submission.title)

    print(cards_master)

    print('done')

    execution_time = (time.time() - start_time)
    print('Execution time in seconds: ' + str(execution_time))

if __name__=="__main__":
    main()
